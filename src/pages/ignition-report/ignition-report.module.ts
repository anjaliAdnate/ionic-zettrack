import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IgnitionReportPage } from './ignition-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    IgnitionReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(IgnitionReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ]
})
export class IgnitionReportPageModule {}
