import { Component } from "@angular/core";
import { ViewController, NavParams, ToastController } from "ionic-angular";
import { DatePicker } from '@ionic-native/date-picker';
import { ApiServiceProvider } from '../../../../../providers/api-service/api-service';
import * as moment from 'moment';

@Component({
    templateUrl: './time-picker.html',
    selector: 'app-time-picker-modal'
})


export class TimePickerModal {
    endTime: any;
    startTime: any;
    islogin: any;
    data: any;
    startDate: string;
    endDate: string;
    startT: Date;
    endT: Date;

    startHourNumber: any;
    startMinutesNumber: any;
    start_ampm: string = "AM";

    endHourNumber: any;
    endMinutesNumber: any;
    end_ampm: string = "AM"

    constructor(navParams: NavParams, public apiCall: ApiServiceProvider, private viewCtrl: ViewController, private datePicker: DatePicker, private toastCtrl: ToastController) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.data = navParams.get('data');

        this.startHourNumber = this.pad(1, 2);
        this.startMinutesNumber = this.pad(0, 2);

        this.endHourNumber = this.pad(1, 2);
        this.endMinutesNumber = this.pad(0, 2);
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    pad(number, length) {

        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }

        return str;

    }

    onHourClicked(key1, key) {
        // debugger
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startHourNumber) < 24) {
                    this.startHourNumber = Number(this.startHourNumber) + 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2)
                } else {
                    this.startHourNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.startHourNumber) > 1) {
                    this.startHourNumber = Number(this.startHourNumber) - 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2)
                } else {
                    this.startHourNumber = this.pad(24, 2)
                }
            }
        } else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endHourNumber) < 24) {
                    this.endHourNumber = Number(this.endHourNumber) + 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2)
                } else {
                    this.endHourNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.endHourNumber) > 1) {
                    this.endHourNumber = Number(this.endHourNumber) - 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2)
                } else {
                    this.endHourNumber = this.pad(24, 2)
                }
            }
        }

    }

    onMinuteClicked(key1, key) {
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startMinutesNumber) < 59) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) + 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2)
                } else {
                    this.startMinutesNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.startMinutesNumber) >= 1) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) - 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2)
                } else {
                    this.startMinutesNumber = this.pad(59, 2)
                }
            }
        } else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endMinutesNumber) < 59) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) + 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2)
                } else {
                    this.endMinutesNumber = this.pad(1, 2);
                }
            } else if (key === 'down') {
                if (Number(this.endMinutesNumber) >= 1) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) - 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2)
                } else {
                    this.endMinutesNumber = this.pad(59, 2)
                }
            }
        }
    }

    ampmClicked(key) {
        if (key === 'start') {
            if (this.start_ampm === "AM") {
                this.start_ampm = "PM";
            } else if (this.start_ampm === "PM") {
                this.start_ampm = "AM";
            }
        } else if (key === 'end') {
            if (this.end_ampm === "AM") {
                this.end_ampm = "PM";
            } else if (this.end_ampm === "PM") {
                this.end_ampm = "AM";
            }
        }
    }

    submit() {
        var startTime, endTime;
        // var st = this.startHourNumber + ":" + this.startMinutesNumber + (this.start_ampm).toLowerCase();
        // var dt = moment(st.toString(), 'h:mm a').format('DD-MM-YYYY h:mm a');
        // console.log(dt);
        // startTime = new Date(dt).toISOString();
        // console.log("start time:", startTime);

        // var et = this.endHourNumber + ":" + this.endMinutesNumber + (this.end_ampm).toLowerCase();
        // var dt1 = moment(et.toString(), 'h:mm a').format('DD-MM-YYYY h:mm a');
        // console.log(dt1);
        // endTime = new Date(dt1).toISOString();
        // console.log("end time: ", endTime);

        var d = new Date();
        d.setHours(Number(this.startHourNumber));
        // var d = new Date();
        d.setMinutes(Number(this.startMinutesNumber));
        startTime = d;

        var d1 = new Date();
        d1.setHours(Number(this.endHourNumber));
        // var d = new Date();
        d1.setMinutes(Number(this.endMinutesNumber));
        endTime = d1;

        console.log("start time:", startTime);
        console.log("end time:", endTime);
        // console.log(d);

        var payload;
        if (this.data.key === 'theft') {
            payload = {
                uid: this.islogin._id,
                theftTime: {
                    start: startTime,
                    end: endTime
                }
            }
        } else if (this.data.key === 'tow') {
            payload = {
                uid: this.islogin._id,
                towTime: {
                    start: startTime,
                    end: endTime
                }
            }
        }

        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe((resp) => {
                console.log('response language code ' + resp);
                this.apiCall.stopLoading();
                let toast = this.toastCtrl.create({
                    message: 'Alert is scheduled sucessfully.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.onDidDismiss(() => {
                    this.dismiss();
                })
                toast.present();

            },
            err => {
                this.apiCall.stopLoading();
                console.log(err);
            });
    }

    // openTimePicker(key) {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'time',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => {
    //             if (key === 'start') {
    //                 this.startTime = moment(new Date(date), 'hh:mm:ss').format("hh:mm a");
    //                 this.startT = date;
    //             } else {
    //                 this.endTime = moment(new Date(date), 'hh:mm:ss').format("hh:mm a");
    //                 this.endT = date;
    //             }
    //             console.log('Got date: ', new Date(date).toISOString())

    //         },
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }

    // submit() {
    //     var payload;
    //     if (this.data.key === 'theft') {
    //         payload = {
    //             uid: this.islogin._id,
    //             theftTime: {
    //                 start: new Date(this.startT).toISOString(),
    //                 end: new Date(this.endT).toISOString()
    //             }
    //         }
    //     } else if (this.data.key === 'tow') {
    //         payload = {
    //             uid: this.islogin._id,
    //             towTime: {
    //                 start: new Date(this.startT).toISOString(),
    //                 end: new Date(this.endT).toISOString()
    //             }
    //         }
    //     }

    //     this.apiCall.startLoading().present();
    //     this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
    //         .subscribe((resp) => {
    //             console.log('response language code ' + resp);
    //             this.apiCall.stopLoading();
    //             let toast = this.toastCtrl.create({
    //                 message: 'Time is set sucessfully.',
    //                 duration: 1500,
    //                 position: 'bottom'
    //             });
    //             toast.onDidDismiss(() => {
    //                 this.dismiss();
    //             })
    //             toast.present();

    //         });
    // }

}