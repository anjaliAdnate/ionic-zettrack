import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateCustModalPage } from './update-cust';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    UpdateCustModalPage
  ],
  imports: [
    IonicPageModule.forChild(UpdateCustModalPage),
    TranslateModule.forChild()
  ]
})
export class UpdateCustModalPageModule {}
